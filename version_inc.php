<?php
$version = trim(file_get_contents('VERSION'));
preg_match('/(.*?)(\d)$/', $version, $matches);
$version = $matches[1] . ($matches[2] + 1);
$file = fopen('VERSION', 'wb');
fwrite($file, $version);
fclose($file);
