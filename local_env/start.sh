#!/bin/bash

service php7.2-fpm start
service nginx start

chmod 777 -R /var/www/html/storage
chmod 777 -R /var/www/html/bootstrap/cache

tail -f /dev/null