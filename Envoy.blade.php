@servers(['production' => 'ubuntu@ec2-34-227-20-248.compute-1.amazonaws.com'])

@setup
    $app_dir = '/home/ubuntu/www/MoneyManager';
    $branch = 'master';
    $composer_app_dir = '/home/ubuntu/www/MoneyManager/application';
@endsetup

@story('deploy')
    fetch_changes
@endstory

@task('fetch_changes')
    echo 'Pulling changes from repository'
    cd {{ $app_dir }}
    git checkout {{ $branch }}
    git pull origin {{ $branch }}
    cd {{ $composer_app_dir }}
    composer update --no-dev
@endtask