
0.7.2 / 2018-10-24
==================

  * Laravel 5.7 update, fixed editing of expected expenses and deleting of transfers

0.7.1 / 2018-10-03
==================

  * Changed behaviour of go back button on transaction page
  * Fixed pagination on storage page
  * Fixed amount displayed for expectation status
  * Fixed styles on expected expenses page, index page and storages page

0.7.0 / 2018-09-25
==================

  * Charts
  * Expected expenses status on index page
  * Latest transactions on index page
  * Added transaction date pick field to transaction add modal
  * Implemented expected expenses
  * Fixed bug when attempting to add transaction group

0.5.2 / 2018-09-18
==================

  * Fixed routes and transaction group creation

0.5.1 / 2018-09-17
==================

  * Added request exceptions handling
  * Added transaction edit feature

0.5.0 / 2018-08-15
==================

  * Docker image build optimization
  * Implemented policies to authorize and validate different user actions
  * Style improvements
  * New navigation menu
  * Implemented transfers between storages
  * Switched action field to varchar from enum


0.4.2 / 2018-08-11
==================

  * Finished filtering and pagination on storage page

0.4.1 / 2018-08-09
==================

  * Fix styles not loading properly

0.4.0 / 2018-08-09
==================

  * Added sort and filter for transactions on storage page

0.3.1 / 2018-07-28
==================

  * Fix of missing input validations

0.3.0 / 2018-07-26
==================

  * Reworked most of the forms, placed them in modal windows.
  * Various fixes and styles changes
  * Excluded master from unit testing


v0.2.4 / 2018-07-24
==================

  * Fixed deleting storage with transactions in it
  * Fixed transaction groups page bug
  * Some other bug fixes

v0.2.3 / 2018-07-23
==================

  * Number fields hotfix

v0.2.2 / 2018-07-23
==================

  * Fixed tests
  * Added styles and layouts

v0.2.1 / 2018-07-23
==================

  * Styles for basic layout, storages and detailed storage pages

v0.2.0 / 2018-07-22
=============

  * Some styles for navigation and transactions pagination
  * CI fine tuning
  * Actual tests and code coverage
  * Refactoring

v0.1.0 / 2018-07-22
===================

  * Basic build of the Money Manager

