<?php

return [
    'default_sort' => env('DEFAULT_TRANSACTIONS_SORT', 'date'),
    'default_sort_by' => env('DEFAULT_TRANSACTIONS_SORT_BY', 'desc'),
    'index_limit' => env('DEFAULT_TRANSACTIONS_LIMIT_ON_INDEX_PAGE', 5),
];