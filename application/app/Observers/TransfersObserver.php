<?php

namespace App\Observers;

use App\Models\Transfers;

class TransfersObserver
{
    public function deleted(Transfers $transfers)
    {
        $transfers->transactionFrom()->first()->delete();
        $transfers->transactionTo()->first()->delete();
    }
}
