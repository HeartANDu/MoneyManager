<?php

namespace App\Observers;

use App\Models\Storage;

class StorageObserver
{
    public function deleting(Storage $storage)
    {
        $storage->transactions()->delete();
    }
}