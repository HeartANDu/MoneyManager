<?php

namespace App\Observers;

use App\Models\Storage;
use App\Models\Transactions;

class TransactionsObserver
{
    public function created(Transactions $transaction)
    {
        $storage = $transaction->storage;
        $storage->balance += $transaction->amount;

        $storage->save();
    }

    public function deleted(Transactions $transaction)
    {
        $storage = $transaction->storage;
        $storage->balance -= $transaction->amount;

        $storage->save();
    }

    public function updating(Transactions $transaction)
    {
        /** @var Storage $originalStorage */
        $originalStorage = resolve(Storage::class)
            ->find($transaction->getOriginal('storage_id'));
        $originalStorage->balance -= $transaction->getOriginal('amount');
        $originalStorage->save();

        $storage = $transaction->storage;
        $storage->balance += $transaction->amount;
        $storage->save();
    }
}
