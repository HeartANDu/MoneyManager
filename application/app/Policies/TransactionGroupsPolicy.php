<?php

namespace App\Policies;

use App\Models\User;
use App\Models\TransactionGroups;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionGroupsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the transaction groups.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\TransactionGroups $transactionGroups
     * @return mixed
     */
    public function view(User $user, TransactionGroups $transactionGroups)
    {
        return $user->id === $transactionGroups->user_id;
    }

    /**
     * Determine whether the user can update the transaction groups.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\TransactionGroups $transactionGroups
     * @return mixed
     */
    public function update(User $user, TransactionGroups $transactionGroups)
    {
        return $user->id === $transactionGroups->user_id
            && in_array($transactionGroups->action, $transactionGroups->getEditableActions());
    }

    /**
     * Determine whether the user can delete the transaction groups.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\TransactionGroups $transactionGroups
     * @return mixed
     */
    public function delete(User $user, TransactionGroups $transactionGroups)
    {
        return $user->id === $transactionGroups->user_id
            && in_array($transactionGroups->action, $transactionGroups->getEditableActions());

    }
}
