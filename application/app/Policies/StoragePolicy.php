<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Storage;
use Illuminate\Auth\Access\HandlesAuthorization;

class StoragePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the storage.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Storage  $storage
     * @return mixed
     */
    public function view(User $user, Storage $storage)
    {
        return $user->id === $storage->user_id;
    }

    /**
     * Determine whether the user can update the storage.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Storage  $storage
     * @return mixed
     */
    public function update(User $user, Storage $storage)
    {
        return $user->id === $storage->user_id;
    }

    /**
     * Determine whether the user can delete the storage.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Storage  $storage
     * @return mixed
     */
    public function delete(User $user, Storage $storage)
    {
        return $user->id === $storage->user_id;
    }
}
