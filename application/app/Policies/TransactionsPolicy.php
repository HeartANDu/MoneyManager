<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Transactions;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the transactions.
     *
     * @param  User  $user
     * @param  Transactions  $transactions
     * @return mixed
     */
    public function view(User $user, Transactions $transactions)
    {
        return $user->id === $transactions->storage()->first()->user_id;
    }

    /**
     * Determine whether the user can update the transactions.
     *
     * @param  User  $user
     * @param  Transactions  $transactions
     * @return mixed
     */
    public function update(User $user, Transactions $transactions)
    {
        return $user->id === $transactions->storage()->first()->user_id;
    }

    /**
     * Determine whether the user can delete the transactions.
     *
     * @param  User  $user
     * @param  Transactions  $transactions
     * @return mixed
     */
    public function delete(User $user, Transactions $transactions)
    {
        return $user->id === $transactions->storage()->first()->user_id;
    }
}
