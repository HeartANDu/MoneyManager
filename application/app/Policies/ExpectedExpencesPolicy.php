<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ExpectedExpenses;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpectedExpencesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the expected expences.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ExpectedExpenses  $expectedExpences
     * @return mixed
     */
    public function view(User $user, ExpectedExpenses $expectedExpences)
    {
        return $expectedExpences->user_id === $user->id;
    }

    /**
     * Determine whether the user can create expected expences.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the expected expences.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ExpectedExpenses  $expectedExpences
     * @return mixed
     */
    public function update(User $user, ExpectedExpenses $expectedExpences)
    {
        return $expectedExpences->user_id === $user->id;
    }

    /**
     * Determine whether the user can delete the expected expences.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ExpectedExpenses  $expectedExpences
     * @return mixed
     */
    public function delete(User $user, ExpectedExpenses $expectedExpences)
    {
        return $expectedExpences->user_id === $user->id;
    }
}
