<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Transfers;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransfersPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the transfers.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Transfers  $transfers
     * @return mixed
     */
    public function view(User $user, Transfers $transfers)
    {
        $from_user_id = $transfers->transactionFrom()->first()->storage()->first()->user_id;
        $to_user_id = $transfers->transactionTo()->first()->storage()->first()->user_id;
        return $user->id === $from_user_id && $user->id === $to_user_id;
    }

    /**
     * Determine whether the user can update the transfers.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Transfers  $transfers
     * @return mixed
     */
    public function update(User $user, Transfers $transfers)
    {
        $from_user_id = $transfers->transactionFrom()->first()->storage()->first()->user_id;
        $to_user_id = $transfers->transactionTo()->first()->storage()->first()->user_id;
        return $user->id === $from_user_id && $user->id === $to_user_id;
    }

    /**
     * Determine whether the user can delete the transfers.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Transfers  $transfers
     * @return mixed
     */
    public function delete(User $user, Transfers $transfers)
    {
        $from_user_id = $transfers->transactionFrom()->first()->storage()->first()->user_id;
        $to_user_id = $transfers->transactionTo()->first()->storage()->first()->user_id;
        return $user->id === $from_user_id && $user->id === $to_user_id;
    }
}
