<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfers extends Model
{
    protected $fillable = ['id_from', 'id_to'];

    public $timestamps = false;

    public function transactionFrom()
    {
        return $this->hasOne(Transactions::class, 'id', 'id_from');
    }

    public function transactionTo()
    {
        return $this->hasOne(Transactions::class, 'id', 'id_to');
    }
}
