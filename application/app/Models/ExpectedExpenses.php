<?php

namespace App\Models;

use App\Entities\ExpectedExpensesEntity;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ExpectedExpenses extends Model
{
    protected $fillable = ['month', 'group_id', 'user_id', 'amount'];

    protected $dates = ['month'];

    protected $dateFormat = 'Y-m-d';

    public $incrementing = false;

    public $timestamps = false;

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where(
                'month',
                '=',
                $this->getAttribute('month')->format($this->dateFormat)
            )
            ->where('group_id', '=', $this->getAttribute('group_id'));

        return $query;
    }

    public function transactionGroup()
    {
        return $this->hasOne(TransactionGroups::class, 'id', 'group_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function monthList()
    {
        return $this
            ->where('user_id', Auth::id())
            ->select('month')
            ->distinct()
            ->get();
    }

    /**
     * @param $month
     * @return Collection
     * @throws \Exception
     */
    public function forMonth($month)
    {
        if (is_string($month)) {
            $month = resolve(Carbon::class)
                ->createFromFormat(ExpectedExpensesEntity::FORMAT, $month)
                ->firstOfMonth();
        } elseif (!$month instanceof Carbon) {
            throw new \Exception('month should be Carbon instance or string');
        }

        return $this
            ->where('user_id', Auth::id())
            ->where('month', $month)
            ->get()
            ->keyBy('group_id');
    }

    public function scopeChart(Builder $query)
    {
        return $query
            ->where('user_id', Auth::id())
            ->whereRaw('year(month) = year(curdate())')
            ->selectRaw('month(month) as date, sum(amount) as amount')
            ->groupBy('date');
    }
}
