<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    protected $fillable = ['name', 'user_id', 'type_id', 'balance'];

    public $timestamps = false;

    public function storageType()
    {
        return $this->hasOne(StorageType::class, 'id', 'type_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transactions::class, 'storage_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
