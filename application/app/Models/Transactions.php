<?php

namespace App\Models;

use App\Traits\ActionValues;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transactions extends Model
{
    use ActionValues;

    const FIELDS = [
        'date' => 'transactions.created_at',
        'action' => 'transactions.action',
        'amount' => 'transactions.amount',
        'group' => 'transaction_groups.name',
    ];

    protected $guarded = ['id', 'updated_at'];

    public function transactionGroup()
    {
        return $this->hasOne(TransactionGroups::class, 'id', 'group_id');
    }

    public function storage()
    {
        return $this->belongsTo(Storage::class, 'storage_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return resolve(Carbon::class)->parse($value)->format(config('view.date_format'));
    }

    public function setAmountAttribute($value)
    {
        $modifier = $this->attributes['action'] === 'subtract' ? -1 : 1;
        $this->attributes['amount'] = $modifier * $value;
    }

    public function scopeStorageQuery(Builder $query)
    {
        $filter = session('transactions.filters', []);
        return $query
            ->where(function (Builder $query) use ($filter) {
                foreach ($filter as $field => $values) {
                    if ($field === 'date' || $field === 'amount') {
                        if (empty($values[0]) || empty($values[1])) {
                            $query->where(
                                self::FIELDS[$field],
                                empty($values[0]) ? '<' : '>',
                                empty($values[0]) ? $values[1] : $values[0]
                            );
                        } else {
                            $query->whereBetween(self::FIELDS[$field], $values);
                        }
                    } elseif (array_key_exists($field, self::FIELDS)) {
                        if (is_array($values)) {
                            $query->whereIn(self::FIELDS[$field], $values);
                        } else {
                            $query->where(self::FIELDS[$field], $values);
                        }
                    }
                }
            })
            ->select('transactions.*', 'transaction_groups.name')
            ->join('transaction_groups', 'transactions.group_id', '=', 'transaction_groups.id');
    }

    public function scopeSorted(Builder $query)
    {
        $sort = self::FIELDS[session('transactions.sort', config('transactions.default_sort'))];
        $sortBy = session('transactions.sort_by', config('transactions.default_sort_by'));
        return $query->orderBy($sort, $sortBy);
    }

    public function scopeStoragePage(Builder $query)
    {
        return $query
            ->storageQuery()
            ->sorted()
            ->paginate(config('view.items_per_page'));
    }

    public function scopeIndexPage(Builder $query)
    {
        return $query
            ->where('transactions.action', '!=', 'transfer')
            ->join('transaction_groups', function ($join) {
                $join->on('transactions.group_id', '=', 'transaction_groups.id')
                    ->where('user_id', Auth::id());
            })
            ->join('storages', 'transactions.storage_id', '=', 'storages.id')
            ->select(['transactions.*', 'transaction_groups.name as group', 'storages.name as storage'])
            ->orderByDesc('created_at')
            ->limit(config('transactions.index_limit'));
    }

    public function scopeChart(Builder $query, string $action)
    {
        return $query
            ->where('transactions.action', $action)
            ->whereRaw('year(transactions.created_at) = year(curdate())')
            ->join('storages', function ($join) {
                $join->on('transactions.storage_id', '=', 'storages.id')
                    ->where('user_id', Auth::id());
            })
            ->selectRaw('month(transactions.created_at) as month, sum(transactions.amount) as amount')
            ->groupBy('month');
    }
}
