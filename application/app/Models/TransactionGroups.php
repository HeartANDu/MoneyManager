<?php

namespace App\Models;

use App\Traits\ActionValues;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransactionGroups extends Model
{
    use ActionValues;

    protected $fillable = ['name', 'user_id', 'action'];

    public $timestamps = false;

    public function transactions()
    {
        return $this->hasMany(Transactions::class, 'group_id', 'id');
    }

    public function scopeDefault(Builder $query)
    {
        return $query
            ->where('user_id', Auth::id())
            ->orderBy('action');
    }

    public function expectedPage()
    {
        return $this
            ->where('user_id', Auth::id())
            ->where('action', 'subtract')
            ->get();
    }

    public function transactionsSum(array $period)
    {
        return -1 * $this
            ->transactions()
            ->whereBetween('created_at', $period)
            ->sum('amount');
    }
}