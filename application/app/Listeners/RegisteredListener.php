<?php

namespace App\Listeners;

use App\Models\TransactionGroups;
use Illuminate\Auth\Events\Registered;

class RegisteredListener
{
    public function handle(Registered $event)
    {
        resolve(TransactionGroups::class)->create([
            'name' => 'Transfer',
            'action' => 'transfer',
            'user_id' => $event->user->id,
        ]);
    }
}
