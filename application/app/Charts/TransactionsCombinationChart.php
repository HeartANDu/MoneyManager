<?php

namespace App\Charts;

class TransactionsCombinationChart implements Chartable
{
    const ACTIONS = ['add', 'subtract'];
    const TITLE = 'Expenses to income chart';

    private $colors = [];

    public function getChart(): array
    {
        $result = [];

        foreach (self::ACTIONS as $action) {
            /** @var Chartable $chart */
            $chart = resolve(TransactionsChart::class);
            $options = ['action' => $action];
            if (isset($this->colors[$action])) {
                $options['color'] = $this->colors[$action];
            }

            $chart->setOptions($options);
            $result[] = $chart->getChart();
        }

        return $result;
    }

    public function getTitle(): string
    {
        return self::TITLE;
    }

    // TODO: To refactor in future if more options will be needed (move to parent class and unify)
    public function setOptions(array $options)
    {
        if (isset($options['color']) && is_array($options['color'])) {
            $colors = $options['color'];
            foreach ($colors as $action => $color) {
                if (!in_array($action, self::ACTIONS)) {
                    unset($colors[$action]);
                }
            }

            $this->colors = $colors;
        }
    }
}
