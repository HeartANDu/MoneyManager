<?php

namespace App\Charts;

use App\Models\ExpectedExpenses;

class ExpectedExpensesChart extends BasicChart
{
    const LABEL = 'Expected expenses';
    const COLOR = '#FFA500';
    const TITLE = 'Expected expenses chart';

    private $expected_expenses;
    private $color;

    public function __construct(ExpectedExpenses $expenses)
    {
        $this->expected_expenses = $expenses;
    }

    function getChartData(): array
    {
        $chart_base = array_fill(1, 12, 0);

        $chart_data = $this->expected_expenses
            ->chart()
            ->get()
            ->pluck('amount', 'date')
            ->toArray();

        foreach ($chart_data as $key => $value) {
            $chart_base[$key] += $value;
        }

        return array_values($chart_base);
    }

    function getDataLabel(): string
    {
        return self::LABEL;
    }

    function getDataColor(): string
    {
        return $this->color ?? self::COLOR;
    }

    public function getTitle(): string
    {
        return self::TITLE;
    }

    public function setOptions(array $options)
    {
        $this->color = $options['color'] ?? null;
    }
}
