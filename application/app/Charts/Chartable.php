<?php

namespace App\Charts;

interface Chartable
{
    public function getChart(): array;

    public function getTitle(): string;

    public function setOptions(array $options);
}
