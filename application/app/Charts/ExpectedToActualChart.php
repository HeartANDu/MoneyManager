<?php

namespace App\Charts;

class ExpectedToActualChart implements Chartable
{
    const TITLE = 'Expected expenses to actual expenses chart';
    const CHARTS = [
        ExpectedExpensesChart::class,
        TransactionsChart::class,
    ];

    private $colors = [];

    public function getChart(): array
    {
        $result = [];

        foreach (self::CHARTS as $chart_name) {
            /** @var Chartable $chart */
            $chart = resolve($chart_name);
            $options = ['action' => 'subtract'];
            if (isset($this->colors[$chart_name])) {
                $options['color'] = $this->colors[$chart_name];
            }

            $chart->setOptions($options);
            $result[] = $chart->getChart();
        }

        return $result;
    }

    public function getTitle(): string
    {
        return self::TITLE;
    }

    // TODO: To refactor in future if more options will be needed (move to parent class and unify)
    public function setOptions(array $options)
    {
        if (isset($options['color']) && is_array($options['color'])) {
            $colors = $options['color'];
            foreach ($colors as $chart => $color) {
                if (!in_array($chart, self::CHARTS)) {
                    unset($colors[$chart]);
                }
            }

            $this->colors = $colors;
        }
    }
}
