<?php

namespace App\Charts;

use App\Models\Transactions;

class TransactionsChart extends BasicChart
{
    const LABEL = [
        'subtract' => 'Expenses',
        'add' => 'Income',
    ];
    const COLORS = [
        'subtract' =>  '#735080',
        'add' => '#7CFC00',
    ];

    private $transactions;
    private $action;
    private $color;

    public function __construct(Transactions $transactions)
    {
        $this->transactions = $transactions;
    }

    public function setOptions(array $options)
    {
        $action = $options['action'];
        if (!in_array($action, $this->transactions->getEditableActions())) {
            throw new \Exception('Illegal action provided: ' . $action);
        }

        $this->action = $action;
        $this->color = $options['color'] ?? null;
    }

    public function getChart(): array
    {
        if (empty($this->action)) {
            throw new \Exception('No action specified');
        }

        return parent::getChart();
    }

    public function getTitle(): string
    {
        if (empty($this->action)) {
            throw new \Exception('No action specified');
        }

        return self::LABEL[$this->action] . ' chart';
    }

    public function getChartData(): array
    {
        $chart_base = array_fill(1, 12, 0);

        $chart_data = $this->transactions
            ->chart($this->action)
            ->get()
            ->pluck('amount', 'month')
            ->map(function ($item, $key) {
                return abs($item);
            })
            ->toArray();

        foreach ($chart_data as $key => $value) {
            $chart_base[$key] += $value;
        }

        return array_values($chart_base);
    }

    public function getDataLabel(): string
    {
        return self::LABEL[$this->action];
    }

    public function getDataColor(): string
    {
        return $this->color ?? self::COLORS[$this->action];
    }
}
