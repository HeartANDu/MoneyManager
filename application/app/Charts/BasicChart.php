<?php

namespace App\Charts;

abstract class BasicChart implements Chartable
{
    abstract function getChartData(): array;

    abstract function getDataLabel(): string;

    abstract function getDataColor(): string;

    public function getChart(): array
    {
        return [
            'label' => $this->getDataLabel(),
            'borderColor' => $this->getDataColor(),
            'backgroundColor' => 'transparent',
            'data' => $this->getChartData(),
        ];
    }
}
