<?php

namespace App\Charts;

class ChartService
{
    public function getCharts(array $charts, array $options = []): array
    {
        $result = [];
        foreach ($charts as $chart_class) {
            /** @var Chartable $chart */
            $chart = resolve($chart_class);
            if (isset($options[$chart_class]) ?? !empty($options[$chart_class])) {
                $chart->setOptions($options[$chart_class]);
            }

            $result[] = [
                'title' => $chart->getTitle(),
                'data' => $chart->getChart(),
            ];
        }

        return $result;
    }
}
