<?php

namespace App\Entities;

use App\Models\ExpectedExpenses;
use App\Models\TransactionGroups;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ExpectedExpensesEntity
{
    const FORMAT = 'Y-m';

    private $month;
    /** @var Collection $month_list */
    private $month_list;
    /** @var ExpectedExpenses $model */
    private $model;
    /** @var Collection $groups */
    private $groups;
    /** @var Collection $items */
    private $items;

    public function __construct()
    {
        $this->model = resolve(ExpectedExpenses::class);
        $this->month = $this->prepareMonthValue();
        $this->month_list = $this->model->monthList();
        $this->groups = resolve(TransactionGroups::class)->expectedPage();
    }

    public function add(array $input): self
    {
        foreach ($this->prepareInput($input) as $data) {
            $this->model->create($data);
        }

        return $this;
    }

    public function update(array $input): self
    {
        $collection = $this->model->forMonth($this->month);
        foreach ($this->prepareInput($input) as $data) {
            /** @var ExpectedExpenses $expected */
            $expected = $collection->get($data['group_id']);
            if ($expected !== null) {
                $expected->update($data);
            } elseif ($data['amount'] !== 0) {
                $this->model->create($data);
            }
        }

        return $this;
    }

    public function get(): self
    {
        if (!isset($this->items)) {
            $this->items = $this->collectByGroup();
        }

        return $this;
    }

    public function setMonth($month): self
    {
        $this->month = $this->prepareMonthValue($month);

        return $this;
    }

    public function getMonth(): Carbon
    {
        return $this->month;
    }

    public function getMonthString(string $format = ''): string
    {
        if (empty($format)) {
            $format = self::FORMAT;
        }

        return $this->month->format($format);
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function hasSomeRecords(): bool
    {
        return $this->month_list->isNotEmpty();
    }

    public function hasSelectedMonthRecords(): bool
    {
        return isset($this->items) && $this->items->isNotEmpty();
    }

    private function prepareMonthValue($month = null): Carbon
    {
        $prepared = resolve(Carbon::class)->today();

        if ($month instanceof Carbon) {
            $prepared = $month;
        }

        if (is_string($month) && !empty($month)) {
            $prepared = resolve(Carbon::class)
                ->createFromFormat(self::FORMAT, $month);
        }

        return $prepared
            ->firstOfMonth()
            ->setTime(0, 0, 0);
    }

    private function collectByGroup(): Collection
    {
        $aggregation = [];
        $expected = $this->model->forMonth($this->month);
        $period = [
            $this->month->toDateString(),
            $this->month->copy()->addMonth()->toDateString()
        ];

        if ($expected->isNotEmpty()) {
            /** @var TransactionGroups $group */
            foreach ($this->groups as $group) {
                $expected_amount = $expected->get($group->id)->amount ?? 0;
                $current_amount = $group->transactionsSum($period);
                $aggregation[] = [
                    'expected' => $expected_amount,
                    'current' => $current_amount,
                    'difference' => $expected_amount - $current_amount,
                    'group_id' => $group->id,
                    'group_name' => $group->name,
                ];
            }
        }

        return collect($aggregation)->keyBy('group_id');
    }

    private function prepareInput(array $groups): array
    {
        $result = [];
        foreach ($groups as $group => $amount) {
            if (!empty($amount)) {
                $result[] = [
                    'month' => $this->month,
                    'group_id' => $group,
                    'user_id' => Auth::id(),
                    'amount' => $amount,
                ];
            }
        }

        return $result;
    }
}
