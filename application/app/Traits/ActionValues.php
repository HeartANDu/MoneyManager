<?php

namespace App\Traits;

trait ActionValues
{
    protected $actions = [
        'add',
        'subtract',
        'transfer'
    ];

    protected $protected_actions = [
        'transfer'
    ];

    public function getPossibleActionValues(): array
    {
        return $this->actions;
    }

    public function getEditableActions(): array
    {
        return array_diff($this->actions, $this->protected_actions);
    }
}
