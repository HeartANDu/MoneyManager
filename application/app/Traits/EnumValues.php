<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait EnumValues
{
    static protected $enumValues = [];

    /**
     * Retrieves the acceptable enum fields for a column
     *
     * @param string $column Column name
     *
     * @return array
     */
    public function getPossibleEnumValues($column): array
    {
        if (!isset(self::$enumValues[$column])) {
            $enumStr = DB::select(DB::raw('SHOW COLUMNS FROM ' . $this->getTable() . ' WHERE Field = "' . $column . '"'))[0]->Type;
            preg_match_all("/'([^']+)'/", $enumStr, $matches);
            self::$enumValues[$column] = isset($matches[1]) ? $matches[1] : [];
        }

        return self::$enumValues[$column];
    }
}