<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\URL;

class RequestException extends Exception implements Renderable
{

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        return redirect(URL::previous())->withErrors(['error' => $this->getMessage()]);
    }
}
