<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\Transactions;
use Illuminate\Foundation\Http\FormRequest;

class EditTransaction extends FormRequest
{
    public function authorize()
    {
        /** @var Transactions $transaction */
        $transaction = resolve(Transactions::class)->find($this->route('id'));
        $actions = $transaction->getEditableActions();
        return $transaction
            && $this->user()->can('update', $transaction)
            && in_array($transaction->action, $actions);
    }

    public function rules()
    {
        return [];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t edit this transaction', 403);
    }
}
