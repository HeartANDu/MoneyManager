<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use Illuminate\Foundation\Http\FormRequest;

class CreateStorage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'type_id' => 'required|integer|exists:storage_types,id',
            'balance' => 'required|numeric'
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t create this storage', 403);
    }
}
