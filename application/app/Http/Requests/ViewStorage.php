<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\Storage;
use Illuminate\Foundation\Http\FormRequest;

class ViewStorage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $storage = resolve(Storage::class)->find($this->route('id'));
        return $storage && $this->user()->can('view', $storage);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t view this storage', 403);
    }
}
