<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\TransactionGroups;
use Illuminate\Foundation\Http\FormRequest;

class DeleteTransactionGroups extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $group = resolve(TransactionGroups::class)->find($this->group_id);
        return $group && $this->user()->can('delete', $group);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_id' => 'required|integer'
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t delete this transaction group', 403);
    }
}
