<?php

namespace App\Http\Requests;

use App\Models\Storage;
use App\Models\Transactions;
use App\Exceptions\RequestException;
use Illuminate\Foundation\Http\FormRequest;

class SortTransactions extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $storage = resolve(Storage::class)->find($this->storage_id);
        return $storage && $this->user()->can('view', $storage);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'storage_id' => 'required|integer|exists:storages,id',
            'page' => 'integer',
            'sort' => 'required|string|in:' . implode(',', array_keys(Transactions::FIELDS)),
            'sort_by' => 'required|string|in:asc,desc',
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t view this storage', 403);
    }
}
