<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\ExpectedExpenses;
use App\Models\TransactionGroups;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class CreateExpectedExpenses extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $groups_list = array_keys($this->groups);
        /** @var Collection $groups */
        $groups = resolve(TransactionGroups::class)->whereIn('id', $groups_list)->get();
        /** @var Collection $expected */
        $expected = resolve(ExpectedExpenses::class)->where('month', $this->month)->get();
        $auth = true;
        foreach ($groups as $group) {
            if ($group->user_id !== $this->user()->id) {
                $auth = false;
            }
        }

        return $expected->isEmpty() && $groups->isNotEmpty() && $auth;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'month' => 'required|date_format:Y-m',
            'groups.*' => 'nullable|numeric|min:0',
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You do not own some of the groups or you\'ve selected an existing month', 403);
    }
}
