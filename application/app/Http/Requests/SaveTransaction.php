<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\Storage;
use App\Models\TransactionGroups;
use App\Models\Transactions;
use Illuminate\Foundation\Http\FormRequest;

class SaveTransaction extends FormRequest
{
    public function authorize()
    {
        $transaction = resolve(Transactions::class)->find($this->route('id'));
        $storage = resolve(Storage::class)->find($this->get('storage_id'));
        $group = resolve(TransactionGroups::class)->find($this->get('group_id'));
        return $transaction
            && $storage
            && $group
            && $this->user()->can('update', $transaction)
            && $this->user()->can('update', $storage)
            && $this->user()->can('update', $group);
    }

    public function rules()
    {
        return [
            'created_at' => 'required|date_format:Y-m-d',
            'amount' => 'required|numeric|min:0',
            'description' => 'nullable|string|max:255',
            'group_id' => 'required|integer|exists:transaction_groups,id',
            'storage_id' => 'required|integer|exists:storages,id',
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t edit this transaction', 403);
    }
}
