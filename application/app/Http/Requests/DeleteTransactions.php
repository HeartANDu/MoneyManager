<?php

namespace App\Http\Requests;

use App\Models\Storage;
use App\Exceptions\RequestException;
use App\Models\Transactions;
use Illuminate\Foundation\Http\FormRequest;

class DeleteTransactions extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Transactions $transactions, Storage $storage)
    {
        $transaction = $transactions->find($this->id);
        $storage = $storage->find($this->storage_id);
        return $transaction
            && $storage
            && $this->user()->can('delete', $transaction)
            && $this->user()->can('view', $storage);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'storage_id' => 'required|integer'
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t delete this transaction', 403);
    }
}
