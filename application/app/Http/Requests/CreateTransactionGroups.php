<?php

namespace App\Http\Requests;

use App\Models\TransactionGroups;
use App\Exceptions\RequestException;
use Illuminate\Foundation\Http\FormRequest;

class CreateTransactionGroups extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $actions = resolve(TransactionGroups::class)->getEditableActions();
        return [
            'name' => 'required|string|max:255',
            'action' => 'required|string|in:' . implode(',', $actions),
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t create this transaction group', 403);
    }
}
