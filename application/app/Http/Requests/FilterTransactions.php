<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\Storage;
use App\Models\Transactions;
use Illuminate\Foundation\Http\FormRequest;

class FilterTransactions extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $storage = resolve(Storage::class)->find($this->storage_id);
        return $storage && $this->user()->can('view', $storage);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $actions = resolve(Transactions::class)->getPossibleActionValues();
        return [
            'storage_id' => 'required|integer|exists:storages,id',
            'filter' => 'array',
            'filter.date_from' => 'date_format:Y-m-d',
            'filter.date_to' => 'date_format:Y-m-d',
            'filter.amount_from' => 'numeric',
            'filter.amount_to' => 'numeric',
            'filter.action' => 'string|in:'. implode(',', $actions),
            'filter.group.*' => 'string'
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t view this storage', 403);
    }
}
