<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\Storage;
use App\Models\Transactions;
use Illuminate\Foundation\Http\FormRequest;

class CreateTransactions extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $storage = resolve(Storage::class)->find($this->storage_id);
        return $storage && $this->user()->can('update', $storage);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $actions = resolve(Transactions::class)->getPossibleActionValues();
        return [
            'action' => 'required|string|in:' . implode(',', $actions),
            'amount' => 'required|numeric',
            'created_at' => 'nullable|date_format:Y-m-d',
            'description' => 'nullable|string|max:255',
            'group_id' => 'required|integer|exists:transaction_groups,id',
            'storage_id' => 'required|integer|exists:storages,id',
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t create this transaction', 403);
    }
}
