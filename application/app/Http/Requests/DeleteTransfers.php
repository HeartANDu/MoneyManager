<?php

namespace App\Http\Requests;

use App\Models\Storage;
use App\Exceptions\RequestException;
use App\Models\Transfers;
use Illuminate\Foundation\Http\FormRequest;

class DeleteTransfers extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $transfer = resolve(Transfers::class)
            ->where('id_from', $this->id)
            ->orWhere('id_to', $this->id)
            ->first();
        $storage = resolve(Storage::class)
            ->find($this->storage_id);
        return $transfer
            && $storage
            && $this->user()->can('delete', $transfer)
            && $this->user()->can('view', $storage);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'storage_id' => 'required|integer'
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t delete this transaction', 403);
    }
}
