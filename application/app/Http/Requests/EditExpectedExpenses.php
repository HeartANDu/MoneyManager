<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\ExpectedExpenses;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class EditExpectedExpenses extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var Collection $expected */
        $expected = resolve(ExpectedExpenses::class)->forMonth($this->month);

        return $expected->isNotEmpty();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('There\'s no expected expenses for selected month', 403);
    }
}
