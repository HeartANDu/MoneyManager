<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\Transactions;
use Illuminate\Foundation\Http\FormRequest;

class ViewTransaction extends FormRequest
{
    public function authorize()
    {
        $transaction = resolve(Transactions::class)->find($this->route('id'));
        return $transaction && $this->user()->can('view', $transaction);
    }

    public function rules()
    {
        return [];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t view this transaction', 403);
    }
}
