<?php

namespace App\Http\Requests;

use App\Exceptions\RequestException;
use App\Models\Storage;
use App\Models\TransactionGroups;
use Illuminate\Foundation\Http\FormRequest;

class CreateTransfers extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $storage_from = resolve(Storage::class)->find($this->storage_id);
        $storage_to = resolve(Storage::class)->find($this->transfer_id);
        $group = resolve(TransactionGroups::class)->find($this->group_id);
        return $storage_from
            && $storage_to
            && $group
            && $this->user()->can('update', $storage_from)
            && $this->user()->can('update', $storage_to)
            && $this->user()->can('view', $group);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'action' => 'required|string|in:transfer',
            'amount' => 'required|numeric',
            'description' => 'nullable|string|max:255',
            'group_id' => 'required|integer',
            'storage_id' => 'required|integer',
            'transfer_id' => 'required|integer',
        ];
    }

    /**
     * @throws RequestException
     */
    protected function failedAuthorization()
    {
        throw new RequestException('You can\'t create this transaction', 403);
    }
}
