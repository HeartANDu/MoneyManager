<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransfers;
use App\Http\Requests\DeleteTransfers;
use App\Models\Storage;
use App\Models\Transactions;
use App\Models\Transfers;
use Illuminate\Http\Request;

class TransfersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(CreateTransfers $request)
    {
        $input = $this->prepareAddInput($request);
        $from = resolve(Transactions::class)->create($input[0]);
        $to = resolve(Transactions::class)->create($input[1]);
        resolve(Transfers::class)->create([
            'id_from' => $from->id,
            'id_to' => $to->id,
        ]);
        return response(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function delete(DeleteTransfers $request)
    {
        $input = $request->validated();
        /** @var Transfers $transfer */
        $transfer = resolve(Transfers::class)
            ->where('id_from', $input['id'])
            ->orWhere('id_to', $input['id'])
            ->first();
        $transfer->delete();
        $storage = resolve(Storage::class)->find($input['storage_id']);

        return response([
            'transactions' => $storage
                ->transactions()
                ->storagePage(),
            'balance' => $storage->balance,
        ]);
    }

    private function prepareAddInput(CreateTransfers $request): array
    {
        $input = $request->validated();

        return [
            [
                'action' => $input['action'],
                'amount' => -1 * $input['amount'],
                'description' => $input['description'],
                'group_id' => $input['group_id'],
                'storage_id' => $input['storage_id'],
            ],
            [
                'action' => $input['action'],
                'amount' => $input['amount'],
                'description' => $input['description'],
                'group_id' => $input['group_id'],
                'storage_id' => $input['transfer_id'],
            ]
        ];
    }
}
