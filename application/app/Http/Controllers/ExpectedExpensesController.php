<?php

namespace App\Http\Controllers;

use App\Entities\ExpectedExpensesEntity;
use App\Http\Requests\CreateExpectedExpenses;
use App\Http\Requests\EditExpectedExpenses;
use App\Http\Requests\SaveExpectedExpenses;
use App\Http\Requests\ViewExpectedExpences;
use App\Models\TransactionGroups;

class ExpectedExpensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(ViewExpectedExpences $request)
    {
        return view('pages.expected_expenses.view', [
            'expected' => resolve(ExpectedExpensesEntity::class)
                ->setMonth($request->month)
                ->get(),
        ]);
    }

    public function add(CreateExpectedExpenses $request)
    {
        $input = $request->validated();
        resolve(ExpectedExpensesEntity::class)
            ->setMonth($input['month'])
            ->add($input['groups']);

        return redirect(route('view_expected_expenses', ['month' => $input['month']]));
    }

    public function editPage(EditExpectedExpenses $request)
    {
        return view('pages.expected_expenses.edit', [
            'expected' => resolve(ExpectedExpensesEntity::class)
                ->setMonth($request->month)
                ->get(),
        ]);
    }

    public function createPage()
    {
        $groups = resolve(TransactionGroups::class)->expectedPage();
        return view('pages.expected_expenses.create', [
            'groups' => $groups,
        ]);
    }

    public function save(SaveExpectedExpenses $request)
    {
        $input = $request->validated();
        resolve(ExpectedExpensesEntity::class)
            ->setMonth($request->month)
            ->update($input['groups']);

        return redirect(route('view_expected_expenses', ['month' => $request->month]));
    }
}
