<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditTransaction;
use App\Http\Requests\SaveTransaction;
use App\Http\Requests\ViewTransaction;
use App\Models\Storage;
use App\Models\TransactionGroups;
use App\Models\Transactions;
use Illuminate\Support\Facades\Auth;

class TransactionDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(ViewTransaction $request)
    {
        $transaction = resolve(Transactions::class)
            ->find($request->route('id'));

        return view('pages.transaction.view', [
            'transaction' => $transaction
        ]);
    }

    public function edit(EditTransaction $request)
    {
        $transaction = resolve(Transactions::class)
            ->find($request->route('id'));
        $groups = resolve(TransactionGroups::class)
            ->where('user_id', Auth::id())
            ->where('action', $transaction->action)
            ->get();
        $storages = resolve(Storage::class)
            ->where('user_id', Auth::id())
            ->get();

        return view('pages.transaction.edit', [
            'transaction' => $transaction,
            'groups' => $groups,
            'storages' => $storages,
        ]);
    }

    public function save(SaveTransaction $request)
    {
        $input = $request->validated();
        /** @var Transactions $transaction */
        $transaction = resolve(Transactions::class)
            ->find($request->route('id'));

        if ($input['created_at'] === $transaction->created_at) {
            unset($input['created_at']);
        }

        $transaction->update($input);

        return redirect(route('view_transaction', ['id' => $request->route('id')]));
    }
}
