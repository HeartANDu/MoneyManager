<?php

namespace App\Http\Controllers;

use App\Charts\ChartService;
use App\Charts\ExpectedToActualChart;
use App\Charts\TransactionsCombinationChart;
use App\Entities\ExpectedExpensesEntity;
use App\Models\Storage;
use App\Models\Transactions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        if (!Auth::guest()) {
            /** @var Collection $transactions */
            $transactions = resolve(Transactions::class)
                ->indexPage()
                ->get();
            $expected = resolve(ExpectedExpensesEntity::class)
                ->get();
            $balance = resolve(Storage::class)
                ->where('user_id', Auth::id())
                ->sum('balance');
            $chart_data = resolve(ChartService::class)
                ->getCharts(
                    [
                        TransactionsCombinationChart::class,
                        ExpectedToActualChart::class,
                    ]
                );
            return view('pages.index', [
                'transactions' => $transactions,
                'expected' => $expected,
                'balance' => $balance,
                'charts' => $chart_data,
            ]);
        }

        return view('pages.index');
    }
}
