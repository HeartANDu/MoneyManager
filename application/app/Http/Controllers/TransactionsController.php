<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransactions;
use App\Http\Requests\DeleteTransactions;
use App\Http\Requests\FilterTransactions;
use App\Http\Requests\SortTransactions;
use App\Models\Storage;
use App\Models\Transactions;
use Carbon\Carbon;

class TransactionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sort(SortTransactions $request)
    {
        $input = $this->prepareSortInput($request);
        /** @var Storage $storage */
        $storage = resolve(Storage::class)->find($input['storage_id']);

        return response([
            'transactions' => $storage
                ->transactions()
                ->storagePage(),
            'balance' => $storage->balance,
        ]);
    }

    public function filter(FilterTransactions $request)
    {
        $input = $this->prepareFilterInput($request);
        /** @var Storage $storage */
        $storage = resolve(Storage::class)->find($input['storage_id']);

        return response([
            'transactions' => $storage
                ->transactions()
                ->storagePage(),
            'balance' => $storage->balance,
            'filtered_total' => $storage
                ->transactions()
                ->storageQuery()
                ->sum('amount'),
        ]);
    }

    public function add(CreateTransactions $request)
    {
        $input = $request->validated();
        /** @var Carbon $now */
        $now = resolve(Carbon::class);
        // If created_at is today, replace with Carbon instance to track creation time
        if ($input['created_at'] === $now->format('Y-m-d')) {
            $input['created_at'] = $now;
        }

        resolve(Transactions::class)->create($input);
        return response(['status' => 'ok']);
    }

    public function delete(DeleteTransactions $request)
    {
        $input = $request->validated();
        $transaction = resolve(Transactions::class)->find($input['id']);
        $transaction->delete();

        return response([
            'transactions' => resolve(Transactions::class)
                ->where('storage_id', $transaction->storage_id)
                ->storagePage(),
            'balance' => $transaction->storage->balance,
        ]);
    }

    private function prepareSortInput(SortTransactions $request): array
    {
        $input = $request->validated();
        session(['transactions.sort' => $input['sort']]);
        session(['transactions.sort_by' => $input['sort_by']]);
        return $input;
    }

    private function prepareFilterInput(FilterTransactions $request): array
    {
        $input = $request->validated();

        if (array_key_exists('filter', $input)) {
            if (array_key_exists('date_from', $input['filter'])
                || array_key_exists('date_to', $input['filter'])
            ) {
                $input['filter']['date'] = $this->prepareDateFilter(
                    $input['filter']['date_from'] ?? '',
                    $input['filter']['date_to'] ?? ''
                );
            }

            if (array_key_exists('amount_from', $input['filter'])
                || array_key_exists('amount_to', $input['filter'])
            ) {
                $input['filter']['amount'] = [
                    $input['filter']['amount_from'] ?? '',
                    $input['filter']['amount_to'] ?? ''
                ];
            }
        }

        session(['transactions.filters' => $input['filter']]);

        return $input;
    }

    private function prepareDateFilter(string $from, string $to): array
    {
        /** @var Carbon $date_from */
        $date_from = $from
            ? resolve(Carbon::class)
                ->createFromFormat('Y-m-d', $from)
                ->setTime(0, 0, 0)
            : '';
        /** @var Carbon $date_to */
        $date_to = $to
            ? resolve(Carbon::class)
                ->createFromFormat('Y-m-d', $to)
                ->setTime(0, 0, 0)
                ->addDay()
            : '';
        return [$date_from, $date_to];
    }
}