<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransactionGroups;
use App\Http\Requests\DeleteTransactionGroups;
use App\Models\TransactionGroups;
use Illuminate\Support\Facades\Auth;

class TransactionGroupsController extends Controller
{
    protected $transactionGroups;

    public function __construct(TransactionGroups $transactionGroups)
    {
        $this->middleware('auth');
        $this->transactionGroups = $transactionGroups;
    }

    public function index()
    {
        $actions = $this->transactionGroups->getEditableActions();
        $groups = $this->transactionGroups
            ->where('user_id', Auth::id())
            ->whereIn('action', $actions)
            ->get()
            ->sortBy('action')
            ->groupBy('action')
            ->toArray();
        return view(
            'pages.transaction_groups',
            [
                'groups' => $groups,
                'actions' => $actions
            ]
        );
    }

    public function add(CreateTransactionGroups $request)
    {
        $input = $this->validateAddInput($request);
        $this->transactionGroups->create($input);
        return response(['status' => 'ok']);
    }

    public function delete(DeleteTransactionGroups $request)
    {
        $input = $request->validated();
        $group = $this->transactionGroups->find($input['group_id']);
        $group->delete();
        return redirect(route('transaction_groups'));
    }

    private function validateAddInput(CreateTransactionGroups $request): array
    {
        $input =  $request->validated();
        $input['user_id'] = Auth::id();
        return $input;
    }
}