<?php

namespace App\Http\Controllers;

use App\Http\Requests\ViewStorage;
use App\Models\Storage;
use App\Models\TransactionGroups;
use App\Models\Transactions;
use Illuminate\Support\Facades\Auth;

class DetailedStorageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(ViewStorage $request)
    {
        $id = $request->route('id');
        /** @var Storage $storage */
        $storage = resolve(Storage::class)->find($id);

        session(['transactions.filters' => []]);
        session(['last_storage_id' => $id]);

        return view(
            'pages.storage',
            [
                'storageId' => $id,
                'storageName' => $storage->name,
                'transactions' => $storage
                    ->transactions()
                    ->storagePage(),
                'modalData' => $this->getModalData($id),
                'actions' => resolve(Transactions::class)->getPossibleActionValues(),
                'balance' => $storage->balance,
                'sort' => session('transactions.sort', config('transactions.default_sort')),
                'sortBy' => session('transactions.sort_by', config('transactions.default_sort_by')),
                'filter' => $this->getFilterData(),
            ]
        );
    }

    private function getFilterData(): array
    {
        return [
            'amount_from' => resolve(Transactions::class)->storageQuery()->min('amount'),
            'amount_to' => resolve(Transactions::class)->storageQuery()->max('amount'),
            'actions' => resolve(Transactions::class)->getPossibleActionValues(),
            'groups' => resolve(TransactionGroups::class)
                ->default()
                ->get()
                ->toArray(),
        ];
    }

    private function getModalData($id): array
    {
        return [
            'storage_id' => (int) $id,
            'transaction_groups' => resolve(TransactionGroups::class)
                ->default()
                ->get()
                ->groupBy('action')
                ->toArray(),
            'storages' => resolve(Storage::class)
                ->where('user_id', Auth::id())
                ->select('id', 'name')
                ->get()
                ->toArray(),
        ];
    }
}