<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStorage;
use App\Http\Requests\DeleteStorage;
use App\Models\Storage;
use App\Models\StorageType;
use Illuminate\Support\Facades\Auth;

class StorageController extends Controller
{
    protected $storage;
    protected $storageType;

    public function __construct(Storage $storage, StorageType $storageType)
    {
        $this->middleware('auth');
        $this->storage = $storage;
        $this->storageType = $storageType;
    }

    public function index()
    {
        $storages = $this->storage->where('user_id', Auth::id())->get();
        $modalData = [
            'route' => route('add_storage'),
            'storage_types' => $this->storageType->all()
        ];
        return view(
            'pages.storages',
            [
                'modalData' => $modalData,
                'storages' => $storages,
            ]
        );
    }

    public function insert(CreateStorage $request)
    {
        $input = $this->prepareInsertInput($request);
        $this->storage->create($input);
        return response(['status' => 'ok']);
    }

    public function delete(DeleteStorage $request)
    {
        $input = $request->validated();
        $storage = $this->storage->find($input['id']);
        $storage->delete();
        return redirect(route('storages'));
    }

    private function prepareInsertInput(CreateStorage $request): array
    {
        $input = $request->validated();
        $input['user_id'] = Auth::id();
        return $input;
    }
}
