<?php

namespace App\Providers;

use App\Entities\ExpectedExpensesEntity;
use App\Models\Storage;
use App\Models\Transfers;
use App\Observers\StorageObserver;
use App\Observers\TransactionsObserver;
use App\Models\Transactions;
use App\Observers\TransfersObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        resolve(Transactions::class)->observe(TransactionsObserver::class);
        resolve(Storage::class)->observe(StorageObserver::class);
        resolve(Transfers::class)->observe(TransfersObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ExpectedExpensesEntity::class, function ($app) {
            return new ExpectedExpensesEntity();
        });
    }
}
