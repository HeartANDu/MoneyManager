<?php

namespace App\Providers;

use App\Models\Storage;
use App\Models\TransactionGroups;
use App\Models\Transactions;
use App\Models\Transfers;
use App\Policies\StoragePolicy;
use App\Policies\TransactionGroupsPolicy;
use App\Policies\TransactionsPolicy;
use App\Policies\TransfersPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Storage::class => StoragePolicy::class,
        TransactionGroups::class => TransactionGroupsPolicy::class,
        Transactions::class => TransactionsPolicy::class,
        Transfers::class => TransfersPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
