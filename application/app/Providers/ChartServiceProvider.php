<?php

namespace App\Providers;

use App\Charts\ChartService;
use App\Charts\TransactionsChart;
use App\Charts\TransactionsCombinationChart;
use App\Models\Transactions;
use Illuminate\Support\ServiceProvider;

class ChartServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(ChartService::class, function ($app) {
            return new ChartService();
        });
    }
}
