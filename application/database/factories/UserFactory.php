<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Models\StorageType::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word
    ];
});

$factory->define(\App\Models\Storage::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'type_id' => function() {
            return factory(\App\Models\StorageType::class)->create()->id;
        },
        'user_id' => function() {
            return factory(\App\Models\User::class)->create()->id;
        },
        'balance' => $faker->randomFloat(2, 0, 10000000),
    ];
});

$factory->define(\App\Models\TransactionGroups::class, function (Faker $faker) {
    $actions = ['add', 'subtract'];
    return [
        'name' => $faker->word,
        'user_id' => function() {
            return factory(\App\Models\User::class)->create()->id;
        },
        'action' => $actions[$faker->numberBetween(0, 1)]
    ];
});

$factory->define(\App\Models\Transactions::class, function (Faker $faker) {
    $actions = ['add', 'subtract'];
    return [
        'action' => $actions[$faker->numberBetween(0, 1)],
        'amount' => $faker->randomFloat(2, 0, 10000000),
        'description' => $faker->sentence,
        'group_id' => function() {
            return factory(\App\Models\TransactionGroups::class)->create()->id;
        },
        'storage_id' => function() {
            return factory(\App\Models\Storage::class)->create()->id;
        }
    ];
});
