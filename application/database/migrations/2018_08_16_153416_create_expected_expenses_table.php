<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpectedExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expected_expenses', function (Blueprint $table) {
            $table->date('month');
            $table->integer('group_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->float('amount');
            $table->primary(['month', 'group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expected_expenses');
    }
}
