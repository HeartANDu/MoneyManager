<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('action', 20);
            $table->float('amount', 10, 2);
            $table->string('description')->nullable();
            $table->integer('group_id')->unsigned();
            $table->integer('storage_id')->unsigned();
            $table->foreign('storage_id')->references('id')->on('storages');
            $table->foreign('group_id')->references('id')->on('transaction_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
