<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Transactions;

class ModifyTransactionsAmountColumnData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var Transactions $transaction */
        foreach (Transactions::all() as $transaction) {
            $modifier = $transaction->action === 'subtract' ? -1 : 1;
            $transaction->update([
                'amount' => $transaction->amount,
            ]);
            $transaction->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /** @var Transactions $transaction */
        foreach (Transactions::all() as $transaction) {
            $modifier = $transaction->action === 'subtract' ? -1 : 1;
            $transaction->update([
                'amount' => $transaction->amount,
            ]);
            $transaction->save();
        }
    }
}
