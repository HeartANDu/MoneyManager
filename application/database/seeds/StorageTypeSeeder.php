<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StorageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('storage_types')->insert([
            'name' => 'Дебетовая карта',
        ]);
        DB::table('storage_types')->insert([
            'name' => 'Кредитная карта',
        ]);
        DB::table('storage_types')->insert([
            'name' => 'Наличные',
        ]);
    }
}
