<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'IndexController@index')->name('index');

// Storage list routes
Route::get('/storages', 'StorageController@index')->name('storages');
Route::put('/storages', 'StorageController@insert')->name('add_storage');
Route::delete('/storages', 'StorageController@delete')->name('delete_storage');

// Storage routes
Route::get('/storages/{id}', 'DetailedStorageController@index')->name('storage');

// Storage ajax routes
Route::get('/transactions', 'TransactionsController@sort')->name('transactions_page_sort');
Route::post('/transactions', 'TransactionsController@filter')->name('transactions_page_filter');
Route::put('/transactions', 'TransactionsController@add')->name('add_transaction');
Route::delete('/transactions', 'TransactionsController@delete')->name('delete_transaction');

// Detail transactions routes
Route::get('/transactions/{id}', 'TransactionDetailController@index')->name('view_transaction');
Route::post('/transactions/{id}', 'TransactionDetailController@save')->name('save_transaction');
Route::get('/transactions/{id}/edit', 'TransactionDetailController@edit')->name('edit_transaction');

// Transfers routes
Route::put('/transfers', 'TransfersController@add');
Route::delete('/transfers', 'TransfersController@delete');

// Transaction groups routes
Route::get('/groups', 'TransactionGroupsController@index')->name('transaction_groups');
Route::put('/groups', 'TransactionGroupsController@add')->name('add_transaction_group');
Route::delete('/groups', 'TransactionGroupsController@delete')->name('delete_transaction_group');

// Expected expenses routes
Route::get('/expected/create', 'ExpectedExpensesController@createPage')->name('create_expected_expenses');
Route::post('/expected/create', 'ExpectedExpensesController@add')->name('add_expected_expenses');
Route::get('/expected/{month?}', 'ExpectedExpensesController@index')->name('view_expected_expenses');
Route::get('/expected/{month}/edit', 'ExpectedExpensesController@editPage')->name('edit_expected_expenses');
Route::post('/expected/{month}/edit', 'ExpectedExpensesController@save')->name('save_expected_expenses');

Route::get('/unauthorized', 'UnauthorizedController@index')->name('restricted');