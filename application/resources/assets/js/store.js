import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    strict: true,
    state: {
        modal: {
            name: null,
            data: null,
            errors: null
        },
        menu_shown: false
    },
    mutations: {
        updateModal(state, payload) {
            state.modal.name = payload.name;
            state.modal.data = payload.data;
            state.modal.errors = payload.errors;
        },
        updateMenuShown(state, payload) {
            state.menu_shown = payload;
        }
    }
});

export default store;