export default {
    showModal(name, data, errors) {
        this.$store.commit('updateModal', {
            name: name,
            data: data,
            errors: errors
        });
    },
    closeModal() {
        this.$store.commit('updateModal', {
            name: null,
            data: null,
            errors: null
        })
    },
    checkErrors() {
        let errors = window.Laravel.errors;
        if (Object.keys(errors).length > 0) {
            this.showModal('error', null, errors[Object.keys(errors)[0]]);
        }
    },
    logout() {
        window.axios.post('/logout')
            .then(function (response) {
                window.location.href = response.data.url;
            })
            .catch(error => {
                this.$store.commit('updateModal', {
                    name: 'error',
                    data: null,
                    errors: error.toString()
                })
            });
    },
    login(data) {
        window.axios.post('/login', data)
            .then(function(response) {
                window.location.href = response.data.url;
            })
            .catch(error => {
                this.writeErrorsToCurrentModal(error.response.data.errors);
            });
    },
    addStorage(data) {
        window.axios.put('/storages', data)
            .then(function (response) {
                window.location.reload();
            })
            .catch(error => {
                this.writeErrorsToCurrentModal(error.response.data.errors);
            })
    },
    addTransactionGroup(data) {
        window.axios.put('/groups', data)
            .then(function (response) {
                window.location.reload();
            })
            .catch(error => {
                this.writeErrorsToCurrentModal(error.response.data.errors);
            })
    },
    writeErrorsToCurrentModal(error) {
        this.$store.commit('updateModal', {
            name: this.$store.state.modal.name,
            data: this.$store.state.modal.data,
            errors: error
        })
    },
    showMenu() {
        this.$store.commit('updateMenuShown', true);
    },
    hideMenu() {
        if (this.$store.state.menu_shown) {
            this.$store.commit('updateMenuShown', false);
        }
    }
}