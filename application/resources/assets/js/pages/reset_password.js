import Vue from 'vue';
import store from './../store';
import Modals from './../components/Modals';
import tools from './../vue-tools';
import { directive as onClickaway } from 'vue-clickaway';

require('./../bootstrap');

Vue.mixin({
    methods: tools
});

new Vue({
    el: '#app',
    store,
    components: { Modals },
    directives: {
        onClickaway: onClickaway
    },
    mounted: function () {
        this.checkErrors();
    },
});
