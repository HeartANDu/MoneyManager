<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') - Money Manager</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Money Manager">
    <meta name="author" content="HeartANDu">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}?v={{ config('app.version') }}" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico">
    <script>
        window.Laravel = {!! json_encode([
            'errors' => $errors->messages()
        ]) !!};
    </script>
</head>
<body>
    <div class="main-wrapper" id="app">
        <ul class="navigation" :class="{ 'navigation-shown': this.$store.state.menu_shown }" v-on-clickaway="hideMenu">
            <a class="navigation-menu-close" @click.stop="hideMenu"></a>
            <li class="navigation-item navigation-item-username"><span>Money Manager</span></li>
            <li class="navigation-item"><a href="{{ route('index') }}">Index</a></li>
            @guest
                <li class="navigation-item"><a href="{{ route('login') }}" @click.prevent="showModal('login', {{ json_encode(['remember' => old('remember')]) }}, {{ $errors }})">Login</a></li>
                <li class="navigation-item"><a href="{{ route('register') }}">Register</a></li>
            @else
                <li class="navigation-item"><a href="{{ route('storages') }}">Storages</a></li>
                <li class="navigation-item"><a href="{{ route('view_expected_expenses') }}">Expected expenses</a></li>
                <li class="navigation-item"><a href="{{ route('transaction_groups') }}">Transaction groups</a></li>
                <li class="navigation-item"><a href="{{ route('logout') }}" @click.prevent="logout">Logout</a></li>
            @endguest
        </ul>
        <nav class="main-navigation">
            <a class="main-navigation-menu-button" @click.stop="showMenu"></a>
            <div class="main-navigation-user">{{ Auth::guest() ? 'Guest' : Auth::user()->name }}</div>
            <a href="{{ route('index') }}" class="main-navigation-logo">₽</a>
        </nav>
        <main>
            @yield('content')
        </main>
        <modals></modals>
    </div>
    @yield('scripts')
</body>
</html>