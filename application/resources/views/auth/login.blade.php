@extends('layouts.default')

@section('content')
<section>
    <div class="login-wrapper">
        <div class="login-body">
            <h2>{{ __('Login') }}</h2>

            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf

                <div class="login-row">
                    <label for="name" class="login-row-label">{{ __('User name') }}</label>

                    <div class="login-input">
                        <input id="name" type="text" class="form-control" name="name" required autofocus>

                        @if ($errors->has('name'))
                            <span class="login-invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="login-row">
                    <label for="password" class="login-row-label">{{ __('Password') }}</label>

                    <div class="login-input">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="login-invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="login-row">
                    <div class="login-row-label">
                        <div class="login-row-remember-me-checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="login-row">
                    <div class="login-row-actions">
                        <button type="submit" class="login-row-actions-button">
                            {{ __('Login') }}
                        </button>

                        <a class="login-row-actions-forgot-password" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
