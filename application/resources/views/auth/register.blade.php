@extends('layouts.default')

@section('content')
<section>
    <div class="register-wrapper">
        <div class="register-body">
            <h2>{{ __('Register') }}</h2>

            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" class="register-form">
                @csrf

                <div class="register-row">
                    <div class="register-row-label">
                        <label for="name">{{ __('Name') }}</label>
                    </div>
                    <div class="register-row-input">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="register-invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="register-row">
                    <div class="register-row-label">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                    </div>
                    <div class="register-row-input">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="register-invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="register-row">
                    <div class="register-row-label">
                        <label for="password" class="register-row-label">{{ __('Password') }}</label>
                    </div>
                    <div class="register-row-input">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="register-invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="register-row">
                    <div class="register-row-label">
                        <label for="password-confirm" class="register-row-label">{{ __('Confirm Password') }}</label>
                    </div>
                    <div class="register-row-input">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="register-row register-row-button">
                    <button type="submit">
                        {{ __('Register') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script src="/js/register.js?v={{ config('app.version') }}"></script>
@stop
