@extends('layouts.default')

@section('content')
<section>
    <div class="reset-password-wrapper">
        <div class="reset-password-body">
            <h2>{{ __('Reset Password') }}</h2>

            <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}" class="reset-password-form">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="reset-password-row">
                    <div class="reset-password-row-label">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                    </div>
                    <div class="reset-password-row-input">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <div class="reset-password-error" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="reset-password-row">
                    <div class="reset-password-row-label">
                        <label for="password">{{ __('Password') }}</label>
                    </div>
                    <div class="reset-password-row-input">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <div class="reset-password-error" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="reset-password-row">
                    <div class="reset-password-row-label">
                        <label for="password-confirm" class="reset-password-label">{{ __('Confirm Password') }}</label>
                    </div>
                    <div class="reset-password-row-input">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="reset-password-row reset-password-row-button">
                    <button type="submit">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script src="/js/reset_password.js?v={{ config('app.version') }}"></script>
@stop
