@extends('layouts.default')

@section('content')
<section>
    <div class="email-password-wrapper">
        <div class="email-password-body">
            <h2>{{ __('Reset Password') }}</h2>

            @if (session('status'))
                <div class="email-password-alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}" class="email-password-form">
                @csrf

                <div class="email-password-row">
                    <div class="email-password-row-label">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                    </div>
                    <div class="email-password-row-input">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <div class="email-password-error" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="email-password-row email-password-row-button">
                    <button type="submit">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script src="/js/email_password.js?v={{ config('app.version') }}"></script>
@stop