@extends('layouts.default')
@section('title', 'Storage "' . $storageName . '"')
@section('content')
    <section>
        <h2>{{ $storageName }}</h2>
        <transactions
                :transactions_data="{{ json_encode($transactions) }}"
                :initial_balance="{{ $balance }}"
                :init_sort="'{{ $sort }}'"
                :init_sort_by="'{{ $sortBy }}'"
                :filter_data="{{ json_encode($filter) }}"
        >
        </transactions>
        <div class="storage-transactions-add">
            <a @click.prevent="showModal('add-transaction', {{ json_encode($modalData) }}, {{ $errors }})">+ Add transaction</a>
        </div>
    </section>
@stop
@section('scripts')
    <script src="/js/transactions.js?v={{ config('app.version') }}"></script>
@stop