@extends('layouts.default')
@section('title', 'Storages list')
@section('content')
    <section>
        <h2>Storages</h2>
        <div class="storages-wrapper">
            @if ($storages->count() > 0)
            <div class="storages-row storages-header">
                <div class="storages-column">
                    Storage name
                </div>
                <div class="storages-column">Storage type</div>
                <div class="storages-column">Balance</div>
                <div colspan="2" class="storages-column"></div>
            </div>
            @foreach ($storages as $storage)
                <div class="storages-row">
                    <div class="storages-column"><a href="{{ route('storage', ['id' => $storage->id]) }}">{{ $storage->name }}</a></div>
                    <div class="storages-column">{{ $modalData['storage_types']->find($storage->type_id)->name }}</div>
                    <div class="storages-column currency">{{ number_format($storage->balance, 2) }}</div>
                    <div class="storages-column">
                        <a class="storages-delete-button" href="{{ route('delete_storage') }}" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $storage->id }}').submit();"></a>
                        <form action="{{ route('delete_storage') }}" method="post" id="delete-form-{{ $storage->id }}">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="id" value="{{ $storage->id }}" />
                        </form>
                    </div>
                </div>
            @endforeach
            <div class="storages-row storages-total">
                <div class="storages-column">Total</div>
                <div class="storages-column currency">{{ $storages->sum('balance') }}</div>
            </div>
            @else
                <div class="storages-row storages-no-items">
                    <h3>You have no storages yet.</h3>
                </div>
            @endif
            <div class="storages-add">
                <a @click.prevent="showModal('add-storage', {{ json_encode($modalData) }}, {{ $errors }})">+ Add a storage</a>
            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script src="/js/storages.js?v={{ config('app.version') }}"></script>
@stop