@extends('layouts.default')
@section('title', 'Transaction groups')
@section('content')
    <section>
        <h2>Transaction groups</h2>
        <div class="transaction-groups-items-wrapper">
            <div class="transaction-groups-items-flex">
                @if (!empty($groups))
                    @foreach ($groups as $action => $subgroups)
                    <div class="transaction-groups-items-block">
                        <div class="transaction-groups-items-row transaction-groups-head">
                            <h3>{{ $action }}</h3>
                        </div>
                    @foreach ($subgroups as $group)
                        <div class="transaction-groups-items-row">
                            <div class="transaction-groups-name">
                                {{ $group['name'] }}
                                <a href="{{ route('delete_transaction_group') }}" onclick="event.preventDefault(); document.getElementById('delete-transaction-group-{{ $group['id'] }}').submit();"></a>
                            </div>
                            <form action="{{ route('delete_transaction_group') }}" method="post" id="delete-transaction-group-{{ $group['id'] }}">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="group_id" value="{{ $group['id'] }}" />
                            </form>
                        </div>
                    @endforeach
                    </div>
                    @endforeach
                @else
                    <div class="transaction-groups-items-row">
                        <h3>You have no transaction groups yet.</h3>
                    </div>
                @endif
            </div>
            <div class="transaction-groups-add">
                <a @click.prevent="showModal('add-transaction-group', {{ json_encode($actions) }}, {{ $errors }})">+ Add a group</a>
            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script src="/js/transaction_groups.js?v={{ config('app.version') }}"></script>
@stop