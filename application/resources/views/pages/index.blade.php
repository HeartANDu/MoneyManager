@extends('layouts.default')
@section('title', 'Index')
@section('content')
    <section>
        <h2>Money Manager</h2>
        @if(!Auth::guest())
            <div class="index-balance">Current total balance: <span class="currency index-selection">{{ $balance }}</span></div>
            <div class="index-list-wrapper">
            @if($transactions->isNotEmpty())
                    <h3>Latest {{ config('transactions.index_limit') }} transactions:</h3>
                    <div class="index-list-row index-list-head">
                        <div class="index-list-column">Date</div>
                        <div class="index-list-column">Amount</div>
                        <div class="index-list-column">Group</div>
                        <div class="index-list-column">Storage</div>
                    </div>
                    @foreach($transactions as $transaction)
                        <div class="index-list-row index-list-description">
                            <div class="index-list-column"><a href="{{ route('view_transaction', ['id' => $transaction->id]) }}">{{ $transaction->created_at }}</a></div>
                            <div class="index-list-column currency">{{ $transaction->amount }}</div>
                            <div class="index-list-column">{{ $transaction->group }}</div>
                            <div class="index-list-column"><a href="{{ route('storage', ['id' => $transaction->storage_id]) }}">{{ $transaction->storage }}</a></div>
                            @if(!empty($transaction->description))
                                <div class="index-list-description-popup">
                                    <span>
                                        {{ $transaction->description }}
                                    </span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                    <div class="index-go-to">
                        <a href="{{ route('storages') }}">Go to storages</a>
                    </div>
                @else
                    <div class="index-list-row index-list-no-items">
                        <h3>You have no transactions.</h3>
                        <p class="index-common-text">
                            Check your <a href="{{ route('storages') }}">storages</a> page to add some transactions in desired storage.
                        </p>
                    </div>
                @endif
            </div>
            <expectation-status
                :expectation_items="{{ json_encode($expected->getItems()) }}"
                :expectation_route="'{{ route('view_expected_expenses') }}'">
            </expectation-status>
            <h3 class="index-chart-main-title index-chart-base">Charts:</h3>
            <div class="index-chart-wrapper index-chart-base">
                @foreach($charts as $chart)
                    <div class="index-chart-block">
                        <div class="index-chart-title">{{ $chart['title'] }}:</div>
                        <chart
                            :datasets="{{ json_encode($chart['data']) }}">
                        </chart>
                    </div>
                @endforeach
            </div>
        @else
            <div>
                <a href="{{ route('login') }}" @click.prevent="showModal('login', {{ json_encode(['remember' => old('remember')]) }}, {{ $errors }})">Login</a>
                or <a href="{{ route('register') }}">Register</a>
            </div>
        @endif
    </section>
@stop
@section('scripts')
    <script src="/js/index.js?v={{ config('app.version') }}"></script>
@stop