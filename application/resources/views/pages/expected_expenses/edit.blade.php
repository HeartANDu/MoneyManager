@extends('layouts.default')
@section('title', 'Expected expenses')
@section('content')
    <section>
        <h2>Edit expected expenses for {{ $expected->getMonthString('F Y') }}</h2>
            <div class="expected-expenses-wrapper">
                <div class="expected-expenses-form">
                    <form action="{{ route('save_expected_expenses', ['month' => $expected->getMonthString()]) }}" method="POST">
                        @csrf
                        <table>
                            <h3>Edit amounts:</h3>
                            @foreach($expected->getItems() as $item)
                                <tr class="expected-expenses-form-row">
                                    <td>
                                        <label for="{{ $item['group_id'] }}">
                                            {{ $item['group_name'] }}
                                        </label>
                                    </td>
                                    <td>
                                        <input type="number" step="0.01" name="groups[{{ $item['group_id'] }}]" id="{{ $item['group_id'] }}" value="{{ $item['expected'] }}" autocomplete="off" />
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="expected-expenses-form-row expected-expenses-form-button">
                            <input type="submit" value="Save" />
                        </div>
                    </form>
                </div>
            </div>
    </section>
@stop
@section('scripts')
    <script src="/js/expected_expenses.js?v={{ config('app.version') }}"></script>
@stop