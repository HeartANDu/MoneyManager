@extends('layouts.default')
@section('title', 'Expected expenses')
@section('content')
    <section>
        <h2>Expected expenses</h2>
        <div class="expected-expenses-wrapper">
            @if($expected->hasSomeRecords())
                <expected-month-selector
                        :current_month="'{{ $expected->getMonthString() }}'"
                        :route="'{{ route('view_expected_expenses') }}'"
                ></expected-month-selector>
                @if($expected->hasSelectedMonthRecords())
                    <div class="expected-expenses-table">
                        <h3>{{ $expected->getMonthString('F Y') }}</h3>
                        <div class="expected-expenses-table-row expected-expenses-table-head">
                            <div class="expected-expenses-table-cell expected-expenses-table-group">Group</div>
                            <div class="expected-expenses-table-cell expected-expenses-table-expected">Expected</div>
                            <div class="expected-expenses-table-cell expected-expenses-table-current">Current</div>
                            <div class="expected-expenses-table-cell expected-expenses-table-difference">Difference</div>
                        </div>
                        @foreach($expected->getItems() as $item)
                        <div class="expected-expenses-table-row">
                            <div class="expected-expenses-table-cell expected-expenses-table-group">{{ $item['group_name'] }}</div>
                            <div class="currency expected-expenses-table-cell expected-expenses-table-expected">{{ $item['expected'] }}</div>
                            <div class="currency expected-expenses-table-cell expected-expenses-table-current">{{ $item['current'] }}</div>
                            <div class="currency expected-expenses-table-cell expected-expenses-table-difference">{{ $item['difference'] }}</div>
                        </div>
                        @endforeach
                        <div class="expected-expenses-table-row expected-expenses-table-edit">
                            <div class="expected-expenses-table-cell">
                                <a href="{{ route('edit_expected_expenses', ['month' => $expected->getMonthString()]) }}">Edit</a>
                            </div>
                        </div>
                    </div>
                @else
                    <h3 class="expected-expenses-table-empty">No records for selected month</h3>
                @endif
            @else
                <h3 class="expected-expenses-table-empty">You have no expected expenses yet.</h3>
            @endif
            <a href="{{ route('add_expected_expenses') }}">+ Add expectation</a>
        </div>
    </section>
@stop
@section('scripts')
    <script src="/js/expected_expenses.js?v={{ config('app.version') }}"></script>
@stop