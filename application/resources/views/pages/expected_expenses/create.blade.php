@extends('layouts.default')
@section('title', 'Expected expenses')
@section('content')
    <section>
        <h2>Add expected expenses</h2>
        <div class="expected-expenses-wrapper">
            <div class="expected-expenses-form">
                <form action="{{ route('add_expected_expenses') }}" method="POST">
                    @csrf
                    <table>
                        <h3>Add expectation:</h3>
                        <tr class="expected-expenses-form-row">
                            <td>
                                <label for="month">
                                    Month
                                </label>
                            </td>
                            <td>
                                <input type="month" name="month" id="month" />
                            </td>
                        </tr>
                        @foreach($groups as $group)
                            <tr class="expected-expenses-form-row">
                                <td>
                                    <label for="{{ $group->id }}">
                                        {{ $group->name }}
                                    </label>
                                </td>
                                <td>
                                    <input type="number" step="0.01" name="groups[{{ $group->id }}]" id="{{ $group->id }}" autocomplete="off" />
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="expected-expenses-form-row expected-expenses-form-button">
                        <input type="submit" value="Add" />
                    </div>
                </form>
            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script src="/js/expected_expenses.js?v={{ config('app.version') }}"></script>
@stop