@extends('layouts.default')
@section('title', 'Unauthorized')
@section('content')
    <section>
        <h2>Restricted</h2>
    </section>
@stop
@section('scripts')
    <script src="/js/unauthorized.js?v={{ config('app.version') }}"></script>
@stop