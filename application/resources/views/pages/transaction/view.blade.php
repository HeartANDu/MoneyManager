@extends('layouts.default')
@section('title', 'Transaction detail')
@section('content')
    <section>
        <h2>Transaction #{{ $transaction->id }}</h2>
        <table class="transaction-info">
            <tr class="transaction-row">
                <td class="transaction-info-head">Created at:</td>
                <td class="transaction-info-data">{{ $transaction->created_at }}</td>
            </tr>
            <tr class="transaction-row">
                <td class="transaction-info-head">Last updated:</td>
                <td class="transaction-info-data">{{ $transaction->updated_at }}</td>
            </tr>
            <tr class="transaction-row">
                <td class="transaction-info-head">Amount:</td>
                <td class="transaction-info-data currency">{{ $transaction->amount }}</td>
            </tr>
            <tr class="transaction-row">
                <td class="transaction-info-head">Description:</td>
                <td class="transaction-info-data">{{ $transaction->description }}</td>
            </tr>
            <tr class="transaction-row">
                <td class="transaction-info-head">Group:</td>
                <td class="transaction-info-data">{{ $transaction->transactionGroup->name }}</td>
            </tr>
            <tr class="transaction-row">
                <td class="transaction-info-head">Storage:</td>
                <td class="transaction-info-data">{{ $transaction->storage->name }}</td>
            </tr>
            <tr class="transaction-row">
                <td colspan="2">
                    <div class="transaction-controls">
                        <div class="transaction-action">
                            <a href="{{ route('storage', ['id' => $transaction->storage->id]) }}">Go to storage</a>
                        </div>
                        <div class="transaction-action">
                            <a href="{{ route('edit_transaction', ['id' => $transaction->id]) }}">Edit</a>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </section>
@stop
@section('scripts')
    <script src="/js/transaction.js?v={{ config('app.version') }}"></script>
@stop