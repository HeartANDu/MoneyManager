@extends('layouts.default')
@section('title', 'Edit transaction')
@section('content')
    <section>
        <h2>Editing transaction #{{ $transaction->id }}</h2>
        <form method="post" action="{{ route('save_transaction', ['id' => $transaction->id]) }}" id="transaction_edit_form">
            @csrf
            <table class="transaction-info">
                <tr class="transaction-row">
                    <td class="transaction-info-head">
                        <label for="action">
                            Action:
                        </label>
                    </td>
                    <td class="transaction-info-data">
                        {{ $transaction->action }}
                    </td>
                </tr>
                <tr class="transaction-row">
                    <td class="transaction-info-head">
                        <label for="created_at">
                            Created at:
                        </label>
                    </td>
                    <td class="transaction-info-data">
                        <input type="date" value="{{ $transaction->created_at }}" name="created_at" id="created_at" />
                    </td>
                </tr>
                <tr class="transaction-row">
                    <td class="transaction-info-head">
                        <label for="amount">Amount:</label>
                    </td>
                    <td class="transaction-info-data">
                        <input type="number" step="0.01" value="{{ abs($transaction->amount) }}" name="amount" id="amount" />
                    </td>
                </tr>
                <tr class="transaction-row">
                    <td class="transaction-info-head">
                        <label for="description">
                            Description:
                        </label>
                    </td>
                    <td class="transaction-info-data">
                        <input type="text" value="{{ $transaction->description }}" name="description" id="description" />
                    </td>
                </tr>
                <tr class="transaction-row">
                    <td class="transaction-info-head">
                        <label for="group_id">
                            Group:
                        </label>
                    </td>
                    <td class="transaction-info-data">
                        <select name="group_id" id="group_id">
                            @foreach($groups as $group)
                                <option value="{{ $group->id }}" {{ $transaction->transactionGroup->id !== $group->id ? '' : 'selected' }}>{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr class="transaction-row">
                    <td class="transaction-info-head">
                        <label for="storage_id">
                            Storage:
                        </label>
                    </td>
                    <td class="transaction-info-data">
                        <select name="storage_id" id="storage_id">
                            @foreach($storages as $storage)
                                <option value="{{ $storage->id }}" {{ $transaction->storage->id !== $storage->id ? '' : 'selected' }}>{{ $storage->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr class="transaction-row">
                    <td colspan="2">
                        <div class="transaction-controls">
                            <div class="transaction-action">
                                <a href="{{ route('view_transaction', ['id' => $transaction->id]) }}">
                                    Cancel
                                </a>
                            </div>
                            <div class="transaction-action">
                                <a href="{{ route('save_transaction', ['id' => $transaction->storage->id]) }}"
                                   onclick="event.preventDefault(); document.getElementById('transaction_edit_form').submit();"
                                >
                                    Save
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </section>
@stop
@section('scripts')
    <script src="/js/transaction.js?v={{ config('app.version') }}"></script>
@stop