let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/pages/index.js', 'public/js')
    .js('resources/assets/js/pages/email_password.js', 'public/js')
    .js('resources/assets/js/pages/expected_expenses.js', 'public/js')
    .js('resources/assets/js/pages/reset_password.js', 'public/js')
    .js('resources/assets/js/pages/register.js', 'public/js')
    .js('resources/assets/js/pages/storages.js', 'public/js')
    .js('resources/assets/js/pages/transaction_groups.js', 'public/js')
    .js('resources/assets/js/pages/transaction.js', 'public/js')
    .js('resources/assets/js/pages/transactions.js', 'public/js')
    .js('resources/assets/js/pages/unauthorized.js', 'public/js')
    .less('resources/assets/less/app.less', 'public/css');

mix.webpackConfig({
    resolve: {
        alias: {
            '@modals': path.resolve(__dirname, 'resources/assets/js/components/modals')
        }
    }
});