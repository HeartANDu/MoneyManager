<?php

namespace Unit\Models;

use App\Models\Storage;
use App\Models\StorageType;
use App\Models\Transactions;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class StorageTest extends TestCase
{
    use DatabaseTransactions;

    /** @var User $user */
    protected $user;
    /** @var StorageType $storage_type */
    protected $storage_type;
    /** @var Transactions $transaction */
    protected $transaction;
    /** @var Storage $storage */
    protected $storage;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->storage_type = factory(StorageType::class)->create();
        $this->storage = factory(Storage::class)->create([
            'user_id' => $this->user->id,
            'type_id' => $this->storage_type->id
        ]);
        $this->transaction = factory(Transactions::class)->create([
            'storage_id' => $this->storage->id
        ]);
    }

    public function testUser()
    {
        $user = $this->storage->user;
        $this->assertEquals($this->user->id, $user->id);
    }

    public function testStorageType()
    {
        $storage_type = $this->storage->storageType;
        $this->assertEquals($this->storage_type->id, $storage_type->id);
    }

    public function testTransactions()
    {
        $transactions = $this->storage->transactions->first();
        $this->assertEquals($this->transaction->id, $transactions->id);
    }
}
