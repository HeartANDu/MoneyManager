<?php

namespace Feature;

use Faker\Generator as Faker;
use App\Models\Storage;
use App\Models\StorageType;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class StoragesTest extends TestCase
{
    use DatabaseTransactions;

    /** @var User $user */
    protected $user;
    /** @var StorageType $storage_type */
    protected $storage_type;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->storage_type = factory(StorageType::class)->create();
    }

    public function testAddStorage()
    {
        /** @var Faker $faker */
        $faker = resolve(Faker::class);
        $name = $faker->unique()->word;
        $balance = $faker->randomFloat(2, 0, 100000);
        $response = $this->followingRedirects()->actingAs($this->user)->put('/storages', [
            '_token' => csrf_token(),
            'name' => $name,
            'type_id' => $this->storage_type->id,
            'balance' => $balance
        ]);
        $response->assertStatus(200)->assertViewIs('pages.storages');
        $storage = resolve(Storage::class)->where(['user_id' => $this->user->id])->first();
        $this->assertEquals($name, $storage->name);
        $this->assertEquals($balance, $storage->balance);
    }

    public function testDeleteStorage()
    {
        $storage = factory(Storage::class)->create([
            'user_id' => $this->user->id,
            'type_id' => $this->storage_type->id
        ]);
        $response = $this->followingRedirects()->actingAs($this->user)->delete('/storages', [
            'id' => $storage->id,
            '_token' => csrf_token()
        ]);
        $response->assertStatus(200)->assertViewIs('pages.storages');
        $storage = resolve(Storage::class)->where(['user_id' => $this->user->id])->first();
        $this->assertNull($storage);
    }
}