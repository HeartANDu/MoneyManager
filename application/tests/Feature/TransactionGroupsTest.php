<?php

namespace Feature;

use App\Models\TransactionGroups;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionGroupsTest extends TestCase
{
    use DatabaseTransactions;

    /** @var Faker $faker */
    protected $faker;
    /** @var User $user */
    protected $user;

    protected function setUp()
    {
        parent::setUp();
        $this->faker = resolve(Faker::class);
        $this->user = factory(User::class)->create();
    }

    public function actionDataProvider()
    {
        return [
            ['add'],
            ['subtract']
        ];
    }

    /**
     * @dataProvider actionDataProvider()
     */
    public function testTransactionGroupAdd($action)
    {
        $name = $this->faker->word;
        $response = $this->followingRedirects()->actingAs($this->user)->put('/transactions/groups', [
            '_token' => csrf_token(),
            'name' => $name,
            'action' => $action
        ]);
        $response->assertStatus(200)->assertViewIs('pages.transactions.groups');
        $transaction_group = resolve(TransactionGroups::class)->where(['user_id' => $this->user->id])->first();
        $this->assertEquals($name, $transaction_group->name);
        $this->assertEquals($action, $transaction_group->action);
    }

    /**
     * @dataProvider actionDataProvider()
     */
    public function testTransactionGroupDelete($action)
    {
        $transaction_group = factory(TransactionGroups::class)->create([
            'name' => $this->faker->word,
            'action' => $action,
            'user_id' => $this->user->id
        ]);
        $response = $this->followingRedirects()->actingAs($this->user)->delete('/transactions/groups', [
            '_token' => csrf_token(),
            'group_id' => $transaction_group->id
        ]);
        $response->assertStatus(200)->assertViewIs('pages.transactions.groups');
        $storage = resolve(TransactionGroups::class)->where(['user_id' => $this->user->id])->first();
        $this->assertNull($storage);
    }
}
