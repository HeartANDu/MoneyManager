<?php

namespace Feature;

use App\Models\Transactions;
use Faker\Generator as Faker;
use App\Http\Controllers\TransactionsController;
use App\Models\Storage;
use App\Models\TransactionGroups;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionsControllerTest extends TestCase
{
    use DatabaseTransactions;

    /** @var User $user */
    protected $user;
    /** @var Storage $storage */
    protected $storage;
    /** @var TransactionGroups $transaction_group */
    protected $transaction_group;
    /** @var Faker $faker */
    protected $faker;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->storage = factory(Storage::class)->create(['user_id' => $this->user->id]);
        $this->transaction_group = factory(TransactionGroups::class)->create(['user_id' => $this->user->id]);
        $this->faker = resolve(Faker::class);
    }

    public function actionDataProvider()
    {
        return [
            ['add'],
            ['subtract']
        ];
    }

    /**
     * @dataProvider actionDataProvider()
     */
    public function testAddTransaction($action)
    {
        $amount = $this->faker->randomFloat(2, 0, 1000);
        $description = $this->faker->sentence;
        $storage_balance = $this->storage->balance;
        $response = $this->followingRedirects()->actingAs($this->user)->put('/transactions', [
            '_token' => csrf_token(),
            'action' => $action,
            'amount' => $amount,
            'description' => $description,
            'group_id' => $this->transaction_group->id,
            'storage_id' => $this->storage->id
        ]);
        $response->assertStatus(200)->assertViewIs('pages.storage');
        $transaction = resolve(Transactions::class)->where(['storage_id' => $this->storage->id])->first();
        $storage = resolve(Storage::class)->find($this->storage->id);
        if ($action === 'add') {
            $storage_balance += $amount;
        } elseif ($action === 'subtract') {
            $storage_balance -= $amount;
        }

        $this->assertEquals(round($storage_balance, 2), $storage->balance);
        $this->assertEquals($amount, $transaction->amount);
        $this->assertEquals($action, $transaction->action);
        $this->assertEquals($description, $transaction->description);
    }

    /**
     * @dataProvider actionDataProvider()
     */
    public function testRemoveTransaction($action)
    {
        $storage_balance = $this->storage->balance;
        $transaction = factory(Transactions::class)->create([
            'group_id' => $this->transaction_group->id,
            'storage_id' => $this->storage->id,
            'action' => $action,
        ]);
        $response = $this->followingRedirects()->actingAs($this->user)->delete('/transactions', [
            'id' => $transaction->id
        ]);
        $response->assertStatus(200)->assertViewIs('pages.storage');
        $transactions = resolve(Transactions::class)->where(['storage_id' => $this->storage->id])->first();
        $storage = resolve(Storage::class)->find($this->storage->id);

        $this->assertNull($transactions);
        $this->assertEquals(round($storage_balance, 2), $storage->balance);
    }
}
