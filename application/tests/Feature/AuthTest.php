<?php

namespace Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Generator as Faker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    /** @var User $user */
    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class);
        $this->app->instance(User::class, $this->user);
    }

    public function testLoginSuccessful()
    {
        /** @var User $user */
        $user = resolve(User::class)->create();
        $response = $this->post('/login', [
            'name' => $user->name,
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $response->assertStatus(302)->assertRedirect('/');
        $this->assertAuthenticatedAs($user);
    }

    public function testLoginNotSuccessful()
    {
        /** @var User $user */
        $user = resolve(User::class)->create();
        $response = $this->post('/login', [
            'name' => $user->name,
            'password' => 'notValid PasswOrD',
            '_token' => csrf_token()
        ]);
        $response->assertStatus(302);
        $this->assertGuest();
    }

    public function testLogoutUnauthorized()
    {
        $response = $this->post('/logout');
        $response->assertStatus(302)->assertRedirect('/');
    }

    public function testLogoutAuthorized()
    {
        /** @var User $user */
        $user = resolve(User::class)->create();
        $response = $this->actingAs($user)->post('/logout', [
            '_token' => csrf_token()
        ]);
        $response->assertStatus(302)->assertRedirect('/');
        $this->assertGuest();
    }

    public function testRegistration()
    {
        /** @var Faker $faker */
        $faker = resolve(Faker::class);
        $name = $faker->unique()->name;
        $email = $faker->unique()->safeEmail;
        $pwd = 'secret';
        $response = $this->post('/register', [
            'name' => $name,
            'email' => $email,
            'password' => $pwd,
            'password_confirmation' => $pwd,
            '_token' => csrf_token()
        ]);
        $response->assertStatus(302)->assertRedirect('/');
        $this->assertAuthenticated();
        $this->assertEquals($name, \Auth::user()->name);
        $this->assertEquals($email, \Auth::user()->email);
    }

    //TODO Add registration and login errors tests
}