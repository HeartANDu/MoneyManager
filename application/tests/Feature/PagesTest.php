<?php

namespace Tests\Feature;

use App\Models\Storage;
use App\Models\StorageType;
use App\Models\TransactionGroups;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PagesTest extends TestCase
{
    use DatabaseTransactions;

    /** @var User $user */
    protected $user;
    /** @var Storage $storage */
    protected $storage;
    /** @var StorageType $storage_type */
    protected $storage_type;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->storage_type = factory(StorageType::class)->create();
        $this->storage = factory(Storage::class)->create([
            'user_id' => $this->user->id,
            'type_id' => $this->storage_type->id
        ]);
    }

    public function testIndexPage()
    {
        $response = $this->get('/');
        $response->assertStatus(200)->assertViewIs('pages.index');
    }

    public function testLoginPageUnauthorized()
    {
        $response = $this->get('/login');
        $response->assertStatus(200)->assertViewIs('auth.login');
    }

    public function testLoginPageAuthorized()
    {
        $response = $this->actingAs($this->user)->get('/login');
        $response->assertStatus(302)->assertRedirect('/');
        $this->assertAuthenticatedAs($this->user);
    }

    public function testRegisterPageUnauthorized()
    {
        $response = $this->get('/register');
        $response->assertStatus(200)->assertViewIs('auth.register');
    }

    public function testRegisterPageAuthorized()
    {
        $response = $this->actingAs($this->user)->get('/register');
        $response->assertStatus(302)->assertRedirect('/');
        $this->assertAuthenticatedAs($this->user);
    }

    public function testStoragesPageUnauthorized()
    {
        $response = $this->get('/storages');
        $response->assertStatus(302)->assertRedirect('/unauthorized');
    }

    public function testStoragesPage()
    {
        $response = $this->actingAs($this->user)->get('/storages');
        $response->assertStatus(200)->assertViewIs('pages.storages');
    }

    public function testStoragePage()
    {
        $response = $this->actingAs($this->user)->get('/storages/' . $this->storage->id);
        $response->assertStatus(200)->assertViewIs('pages.storage')->assertViewHas(['storageName' => $this->storage->name]);
    }

    public function testTransactionGroupsPage()
    {
        $response = $this->actingAs($this->user)->get('/transactions/groups');
        $response->assertStatus(200)
            ->assertViewIs('pages.transactions.groups')
            ->assertViewHas(['groups' => resolve(TransactionGroups::class)->where(['user_id' => $this->user->id])->get()->groupBy('action')->toArray()]);
    }

    public function testUnauthorizedPageIfAuthorized()
    {
        $response = $this->actingAs($this->user)->get('/unauthorized');
        $response->assertStatus(302)->assertRedirect('/');
    }

    public function testUnauthorizedPageIfUnauthorized()
    {
        $response = $this->get('/unauthorized');
        $response->assertStatus(200)->assertViewIs('pages.restricted');
    }
}